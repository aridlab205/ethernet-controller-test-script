import socket
import random
import os
import time
import thread

def uploadToDDS1():
    global file
    # Debug variables
    reconstructed = ""
    DDS1Dir = "C:\Users\jason\PycharmProjects\Ethernet GUI\Ethernet-controller-test-script\dds1\\"
    print "Uploading to DDS #1"
    ddsFileName = random.choice(os.listdir(DDS1Dir))  # 'barker_code_13us_dds1.bin'
    print ddsFileName
    ddsFileStats = os.stat(DDS1Dir+ddsFileName)
    chunks = ddsFileStats.st_size / 1024
    print "File will be split in", chunks
    ddsFile = open(DDS1Dir+ddsFileName, 'rb')
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # client_socket.setblocking(0)
    try:
        client_socket.connect(('192.168.1.34', 2223))
        # contents = ddsFile.read()
        # ddsFile = open(ddsFileName, 'rb')
        client_socket.send("DDS1_RESET")

        for i in range(0, chunks + 1):
            ddsFileContent = ddsFile.read(1024)
            client_socket.send("DDS1:" + str(i) + ":" + str(chunks + 1) + ":" + ddsFileContent)
            time.sleep(1)
            reconstructed += ddsFileContent
            print "Checksum: " + str(sum(bytearray(reconstructed)))

        startTime = time.time()
        while 1:
            if time.time() - startTime > 2:
                break

            response = client_socket.recv(20)
            break
        client_socket.close()

    except socket.error, (value, message):
        print "Error connecting. Please reset the board. Error message\n" + message


def uploadToDDS2():
    global file
    # Debug variables
    reconstructed = ""

    print "Uploading to DDS #2"
    DDS2Dir = "C:\Users\jason\PycharmProjects\Ethernet GUI\Ethernet-controller-test-script\dds2\\"
    ddsFileName = random.choice(os.listdir(DDS2Dir))  # 'nlfm_5mhz_13us_dds1_ethernet_frame.bin'
    ddsFileStats = os.stat(DDS2Dir+ddsFileName)
    print ddsFileName
    chunks = ddsFileStats.st_size / 1024
    print "File will be split in", chunks
    ddsFile = open(DDS2Dir+ddsFileName, 'rb')
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # client_socket.setblocking(0)
    try:
        client_socket.connect(('192.168.1.34', 2223))
        # contents = ddsFile.read()
        # ddsFile = open(ddsFileName, 'rb')
        client_socket.send("DDS2_RESET")
        for i in range(0, chunks + 1):
            ddsFileContent = ddsFile.read(1024)
            client_socket.send("DDS2:" + str(i) + ":" + str(chunks + 1) + ":" + ddsFileContent)
            time.sleep(1)
            reconstructed += ddsFileContent
            print "Checksum: " + str(sum(bytearray(reconstructed)))

        startTime = time.time()
        while 1:
            if time.time() - startTime > 2:
                break

        response = client_socket.recv(20)
        client_socket.close()

    except socket.error, (value, message):
        print "Error connecting. Please reset the board. Error message\n" + message

def DDSTestAutomate():
    DDSAutomateTestCount = 0
    while 1:
        DDSAutomateTestCount = DDSAutomateTestCount + 1
        print "----- TEST #"+str(DDSAutomateTestCount)+" BEGIN -----"
        uploadToDDS1()
        print "DDS1 Configured:"
        time.sleep(5)
        uploadToDDS2()
        print "DDS2 Configured"
        time.sleep(5)
        print "----- END TEST #"+str(DDSAutomateTestCount)+" -----\n\n"



thread.start_new_thread(DDSTestAutomate())